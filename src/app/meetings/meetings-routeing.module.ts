import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MeetingsPage } from "./meetings.page";
import { AllMeetingPage } from "./all-meeting/all-meeting.page";
import { MeetingCalendarPage } from "./meeting-calendar/meeting-calendar.page";

const routes: Routes = [
  {
    path: "",
    component: MeetingsPage,
    children: [
      {
        path: "allMeetingRoot",
        component: AllMeetingPage
      },
      {
        path: "meetingCalendarRoot",
        component: MeetingCalendarPage
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeetingsRoutingPageModule {}
