import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingCalendarPage } from './meeting-calendar.page';

describe('MeetingCalendarPage', () => {
  let component: MeetingCalendarPage;
  let fixture: ComponentFixture<MeetingCalendarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingCalendarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingCalendarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
