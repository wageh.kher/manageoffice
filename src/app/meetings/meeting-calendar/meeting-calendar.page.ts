import { ViewChild, Component, OnInit } from "@angular/core";
import bootstrapPlugin from "@fullcalendar/bootstrap";
import { FullCalendarComponent } from "@fullcalendar/angular";
import { EventInput } from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGrigPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction"; // for dateClick
import listPlugin from "@fullcalendar/list";
import timelinePlugin from "@fullcalendar/timeline";
@Component({
  selector: "app-meetingCalendarRoot",
  templateUrl: "./meeting-calendar.page.html",
  styleUrls: ["./meeting-calendar.page.scss"]
})
export class MeetingCalendarPage implements OnInit {
  @ViewChild("meetingCalendar") calendarComponent: FullCalendarComponent;
  ngOnInit() {}
  calendarVisible = true;
  calendarPlugins = [
    dayGridPlugin,
    timeGrigPlugin,
    interactionPlugin,
    listPlugin,
    bootstrapPlugin
  ];
  calendarWeekends = true;
  calendarEvents: EventInput[] = [{ title: "Event Now", start: new Date() }];

  toggleVisible() {
    this.calendarVisible = !this.calendarVisible;
  }

  toggleWeekends() {
    this.calendarWeekends = !this.calendarWeekends;
  }

  gotoPast() {
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate("2000-01-01"); // call a method on the Calendar object
  }

  handleDateClick(arg) {
    if (confirm("Would you like to add an event to " + arg.dateStr + " ?")) {
      this.calendarEvents = this.calendarEvents.concat({
        // add new event data. must create new array
        title: "New Event",
        start: arg.date,
        allDay: arg.allDay
      });
    }
  }
}
