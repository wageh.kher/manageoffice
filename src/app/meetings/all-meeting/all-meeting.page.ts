import { Component, OnInit } from "@angular/core";
import { MeetingVm } from "src/app/models/MeetingVm";
import { MeetingService } from "src/app/providers/meeting.service";

@Component({
  selector: "app-allMeetingRoot",
  templateUrl: "./all-meeting.page.html",
  styleUrls: ["./all-meeting.page.scss"]
})
export class AllMeetingPage implements OnInit {
  meetings: MeetingVm[];
  private pageIndex: number;
  constructor(private meetingService: MeetingService) {
    this.meetings = [];
    this.onFindAllMeetings();
  }
  ngOnInit() {}

  onFindAllMeetings() {
    if (this.meetings.length > 0) {
      this.meetings = [];
    }
    this.pageIndex = 1;
    return this.meetingService
      .FindAllMeetings(this.pageIndex)
      .then((data: any) => {
        if (data.result.length > 0) {
          data.result.map(meeting => this.meetings.push(meeting));
          console.log("::  Get All Meetings ::");
          console.log(this.meetings);
          return this.meetings;
        }
      });
  }

  OnLoadData(event) {
    setTimeout(() => {
      event.target.complete();
      this.pageIndex = this.pageIndex + 1;
      this.meetingService.FindAllMeetings(this.pageIndex).then((data: any) => {
        if (
          data.result == null ||
          data.result.length < this.meetingService.pageSize
        ) {
          event.target.disabled = true;
        }

        if (data.result.length > 0) {
          data.result.map(meeting => this.meetings.push(meeting));

          return this.meetings;
        }
      });
    }, 500);
  }
}
