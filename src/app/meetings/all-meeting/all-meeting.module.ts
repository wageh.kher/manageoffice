import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { TranslateModule } from "@ngx-translate/core";
import { AllMeetingPage } from "./all-meeting.page";
import { MeetingCardComponent } from "../meeting-card/meeting-card.component";

const routes: Routes = [
  {
    path: "",
    component: AllMeetingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AllMeetingPage]
})
export class AllMeetingPageModule {}
