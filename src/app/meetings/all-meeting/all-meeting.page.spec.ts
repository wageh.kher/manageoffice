import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMeetingPage } from './all-meeting.page';

describe('AllMeetingPage', () => {
  let component: AllMeetingPage;
  let fixture: ComponentFixture<AllMeetingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllMeetingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMeetingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
