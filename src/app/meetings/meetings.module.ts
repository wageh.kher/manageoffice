import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";
import { MeetingsPage } from "./meetings.page";
import { MeetingsRoutingPageModule } from "./meetings-routeing.module";
import { TranslateModule } from "@ngx-translate/core";
import { MeetingCardComponent } from "./meeting-card/meeting-card.component";
import { MeetingService } from "../providers/meeting.service";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { OptionsPopoverPageModule } from "../Pages/options-popover/options-popover.module";
import { DirectivesModule } from "../directives/directives.module";
import { MeetingCalendarPage } from "./meeting-calendar/meeting-calendar.page";
import { AllMeetingPage } from "./all-meeting/all-meeting.page";
import { FullCalendarModule } from "@fullcalendar/angular";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    MeetingsRoutingPageModule,
    OptionsPopoverPageModule,
    HttpClientModule,
    DirectivesModule,
    FullCalendarModule
  ],
  declarations: [
    MeetingsPage,
    AllMeetingPage,
    MeetingCalendarPage,
    MeetingCardComponent
  ],
  entryComponents: [MeetingCardComponent],
  providers: [HttpClient, MeetingService]
})
export class MeetingsPageModule {}
