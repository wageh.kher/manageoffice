import { Component, OnInit, Input } from "@angular/core";
import {
  PopoverController,
  ToastController,
  AlertController
} from "@ionic/angular";
import { OptionsPopoverPage } from "src/app/Pages/options-popover/options-popover.page";
import { MeetingVm } from "src/app/models/MeetingVm";
import { MeetingService } from "src/app/providers/meeting.service";
import { TranslateService } from "@ngx-translate/core";
import { configurationProvider } from "src/assets/configurtion";
import { HelperService } from "src/app/providers/heper.service";

@Component({
  selector: "app-meeting-card",
  templateUrl: "./meeting-card.component.html",
  styleUrls: ["./meeting-card.component.scss"]
})
export class MeetingCardComponent implements OnInit {
  @Input() meeting: MeetingVm;
  constructor(
    private popoverCtrl: PopoverController,
    public meetingService: MeetingService,
    public config: configurationProvider,
    public translate: TranslateService,
    private toastCtl: ToastController,
    private alertCtrl: AlertController,
    public helperService: HelperService
  ) {
    // this.meetings = [];
    // this.onFindAllMeetings();
  }

  // onFindAllMeetings() {
  //   debugger;
  //   if (this.meetings.length > 0) {
  //     this.meetings = [];
  //   }
  //   this.pageIndex = 1;
  //   return this.meetingService
  //     .FindAllMeetings(this.pageIndex)
  //     .then((data: any) => {
  //       if (data.result.length > 0) {
  //         data.result.map(meeting => this.meetings.push(meeting));
  //         console.log("::  Get All Meetings ::");
  //         console.log(this.meetings);
  //         return this.meetings;
  //       }
  //     });
  // }
  ngOnInit() {}

  async onToggleOptions(ev: any, meetingModel: MeetingVm) {
    const modal = await this.popoverCtrl.create({
      component: OptionsPopoverPage,
      event: ev,
      componentProps: { meetingViewModal: this.meeting }
    });
    await modal.present();
  }
  onSwipeEvent(e) {
    console.log(e);
  }
}
