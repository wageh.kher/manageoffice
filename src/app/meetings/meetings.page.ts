import { Component, OnInit, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { SwipeTabDirective } from "../directives/swipe-tab.directive";
import { IonTabs } from "@ionic/angular";
@Component({
  selector: "app-meetings",
  templateUrl: "./meetings.page.html",
  styleUrls: ["./meetings.page.scss"]
})
export class MeetingsPage implements OnInit {
  @ViewChild(SwipeTabDirective) swipeTabDirective: SwipeTabDirective;
  @ViewChild("meetingTabs") tabRef: IonTabs;
  constructor(private translate: TranslateService) {}

  ngOnInit() {}

  ionTabsDidChange($event) {
    console.log("[TabsPage] ionTabsDidChange, $event: ", $event);
    this.swipeTabDirective.onTabInitialized($event.tab);
    const currentStatus = $event.tab.replace("Root", "");
    // this.interviewService.setcurrentStatus(
    //   this.config.InterviewStatus[currentStatus == "all" ? null : currentStatus]
    // );
  }

  onTabChange($event) {
    console.log("[TabsPage] onTabChange, $event: ", $event);
    this.tabRef.select($event);
  }

  async onAddMeeting() {
    console.log("Add meetings");
    // const modal = await this.modalCtl.create({
    //   component: AddInterviewComponent,
    //   componentProps: { interviewModal: [] }
    // });
    // await modal.present();
  }
}
