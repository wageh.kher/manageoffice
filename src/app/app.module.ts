import { NgModule, enableProdMode } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy, Router } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import {
  HttpClient,
  HttpClientModule,
  HttpClientJsonpModule
} from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { OptionsPopoverPageModule } from "./Pages/options-popover/options-popover.module";

import { configurationProvider } from "src/assets/configurtion";
import { HTTP } from "@ionic-native/http/ngx";
import { DirectivesModule } from "./directives/directives.module";
import { FormsModule } from "@angular/forms";
import { IonicSelectableModule } from "ionic-selectable";
import { LocalNotifications } from "@ionic-native/local-notifications/ngx";
import { MbscModule } from "../lib/mobiscroll/js/mobiscroll.angular.min.js";

import { Network } from "@ionic-native/network/ngx";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { LocationSelectComponent } from "./Pages/location-select/location-select.component";
import { BackgroundGeolocation } from "@ionic-native/background-geolocation/ngx";
import { InterviewService } from "./providers/interview.service";
import { Connectivity } from "./providers/connectivity.service";
import { GoogleMaps } from "./providers/google-maps.service";
import { HelperService } from "./providers/heper.service";
import { MeetingService } from "./providers/meeting.service";
import { AddInterviewComponent } from "./interviews/add-interview/add-interview.component";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [AppComponent, AddInterviewComponent, LocationSelectComponent],
  entryComponents: [AddInterviewComponent, LocationSelectComponent],
  imports: [
    MbscModule.forRoot({ angularRouter: Router }),
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    IonicSelectableModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    NgbModule.forRoot(),
    OptionsPopoverPageModule,
    DirectivesModule,
    HttpClientJsonpModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpClient,
    InterviewService,
    configurationProvider,
    HTTP,
    LocalNotifications,
    Connectivity,
    GoogleMaps,
    Network,
    Geolocation,
    BackgroundGeolocation,
    HelperService,
    MeetingService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
enableProdMode();
