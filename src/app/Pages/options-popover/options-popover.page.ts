import { Component, OnInit } from "@angular/core";
import {
  ModalController,
  NavParams,
  PopoverController,
  AlertController,
  ToastController
} from "@ionic/angular";

import { AddInterviewComponent } from "src/app/interviews/add-interview/add-interview.component";
import { TranslateService } from "@ngx-translate/core";
import { InterviewVm } from "src/app/models/InterviewVm";
import { InterviewService } from "src/app/providers/interview.service";

@Component({
  selector: "page-options-popover",
  templateUrl: "./options-popover.page.html",
  styleUrls: ["./options-popover.page.scss"]
})
export class OptionsPopoverPage implements OnInit {
  interviewModal: InterviewVm;
  constructor(
    public modalCtl: ModalController,
    private navParams: NavParams,
    private popoverCtl: PopoverController,
    private interviewService: InterviewService,
    public translate: TranslateService,
    private toastCtl: ToastController,
    private alertCtrl: AlertController
  ) {
    this.interviewModal = this.navParams.get("interviewModal");
  }

  ngOnInit() {}
  ionViewDidLoad() {
    console.log("ion View Did Load options popover");
  }
  async close() {
    await this.popoverCtl.dismiss();
  }

  async onDeleteInterview() {
    const alert = await this.alertCtrl.create({
      header: "تأكيد",
      message: "هل تريد الحذف بالفعل ؟",
      buttons: [
        {
          text: "الغاء",
          role: "cancel",
          handler: () => {
            console.log(":: Cancel Delete ! ::");
          }
        },
        {
          text: "موافق",
          handler: () => {
            this.interviewService
              .ChangeStatus(
                this.interviewModal.Id,
                7,
                this.interviewModal.DepartmentId
              )
              .then(async (data: any) => {
                console.log(":: Deleted Done ! ::");
                var messageText = "";
                if (data.result.RowsCount > 0) {
                  this.translate.get("messages").subscribe(async value => {
                    messageText = value.save;
                  });
                  this.popoverCtl.dismiss();
                } else {
                  this.translate.get("messages").subscribe(async value => {
                    messageText = value.fail;
                  });
                }
                const toast = await this.toastCtl.create({
                  message: messageText,
                  duration: 2000
                });
                toast.present();
              });
          }
        }
      ]
    });
    alert.present();
  }
  async onEditInterview() {
    const modal = await this.modalCtl.create({
      component: AddInterviewComponent,
      componentProps: { interviewModal: this.interviewModal }
    });
    await modal.present();
  }
}
