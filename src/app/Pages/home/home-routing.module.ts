import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomePage } from "./home.page";
const routes: Routes = [
  { path: "", redirectTo: "/home/interviewsRoot/allRoot", pathMatch: "full" },
  {
    path: "home",
    component: HomePage,
    children: [
      {
        path: "interviewsRoot",
        children: [
          {
            path: "",
            loadChildren:
              "../../interviews/interviews.module#InterviewsPageModule"
          }
        ]
      },
      {
        path: "meetingsRoot",
        children: [
          {
            path: "",
            loadChildren: "../../meetings/meetings.module#MeetingsPageModule"
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
