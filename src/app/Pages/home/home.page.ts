import { Component } from "@angular/core";
import { NavController } from "@ionic/angular";

import { TranslateService } from "@ngx-translate/core";
import { MeetingsPage } from "../../meetings/meetings.page";
import { InterviewsPage } from "../../interviews/interviews.page";
@Component({
  selector: "page-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  meetingsRoot = MeetingsPage;
  interviewsRoot = InterviewsPage;
  constructor(
    public navCtrl: NavController,
    private translate: TranslateService
  ) {}

  onLogout() {
    //this.navCtrl.push(LoginPage);
  }
}
