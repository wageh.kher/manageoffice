import {
  Component,
  ElementRef,
  ViewChild,
  NgZone,
  OnInit
} from "@angular/core";

import {
  NavController,
  Platform,
  ModalController,
  NavParams
} from "@ionic/angular";
import { GoogleMaps } from "src/app/providers/google-maps.service";
import { InterviewVm } from "src/app/models/InterviewVm";

declare var google;

@Component({
  selector: "page-location-select",
  templateUrl: "location-select.component.html",
  styleUrls: ["location-select.component.scss"]
})
export class LocationSelectComponent implements OnInit {
  @ViewChild("map") mapElement: ElementRef;
  @ViewChild("pleaseConnect") pleaseConnect: ElementRef;
  public interviewModal: InterviewVm;
  latitude: number;
  longitude: number;
  autocompleteService: any;
  placesService: any;
  places: any = [];
  searchDisabled: boolean;
  saveDisabled: boolean;
  location: any;

  constructor(
    public navCtrl: NavController,
    public zone: NgZone,
    public maps: GoogleMaps,
    public platform: Platform,
    public modalCtl: ModalController,
    private navParams: NavParams
  ) {
    this.interviewModal = this.navParams.get("interviewModal");
    this.searchDisabled = true;
    this.saveDisabled = true;
    this.location = {
      lat: this.interviewModal.LocationLat,
      lng: this.interviewModal.LocationLong,
      name: this.interviewModal.LocationDesc
    };
  }

  ngOnInit() {
    let mapLoaded = this.maps
      .init(
        this.mapElement.nativeElement,
        this.pleaseConnect.nativeElement,
        this.location
      )
      .then(() => {
        this.autocompleteService = new google.maps.places.AutocompleteService();
        this.placesService = new google.maps.places.PlacesService(
          this.maps.map
        );
        this.searchDisabled = false;
      });
  }
  selectPlace(place) {
    this.places = [];

    const curentLocation = {
      lat: null,
      lng: null,
      name: place.description == "" ? place.name : place.description
    };

    this.placesService.getDetails({ placeId: place.place_id }, details => {
      this.zone.run(() => {
        //curentLocation.name = details.name;
        curentLocation.lat = details.geometry.location.lat();
        curentLocation.lng = details.geometry.location.lng();
        this.saveDisabled = false;

        this.maps.map.setCenter({
          lat: curentLocation.lat,
          lng: curentLocation.lng
        });

        this.location = curentLocation;
      });
    });
  }

  searchPlace() {
    this.saveDisabled = true;

    if (this.location.name != null && !this.searchDisabled) {
      let config = {
        types: ["geocode"],
        input: this.location.name
      };

      this.autocompleteService.getPlacePredictions(
        config,
        (predictions, status) => {
          if (
            status == google.maps.places.PlacesServiceStatus.OK &&
            predictions
          ) {
            this.places = [];

            predictions.forEach(prediction => {
              this.places.push(prediction);
            });
          }
        }
      );
    } else {
      this.places = [];
    }
  }

  save() {
    this.modalCtl.dismiss(this.location);
  }

  close() {
    this.modalCtl.dismiss();
  }
}
