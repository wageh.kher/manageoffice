import { Injectable } from "@angular/core";
import { configurationProvider } from "src/assets/configurtion";
import { Connectivity } from "./connectivity.service";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import {
  BackgroundGeolocation,
  BackgroundGeolocationConfig,
  BackgroundGeolocationResponse,
  BackgroundGeolocationEvents
} from "@ionic-native/background-geolocation/ngx";
import { Platform } from "@ionic/angular";
declare var google;

@Injectable()
export class GoogleMaps {
  mapElement: any;
  pleaseConnect: any;
  map: any;
  mapInitialised: boolean = false;
  mapLoaded: any;
  mapLoadedObserver: any;
  currentMarker: any;
  location: any;
  onDevice: boolean;
  constructor(
    public connectivityService: Connectivity,
    public geolocation: Geolocation,
    public configure: configurationProvider,
    private backgroundGeolocation: BackgroundGeolocation,
    private platform: Platform
  ) {
    this.onDevice = this.platform.is("cordova");
  }

  init(mapElement: any, pleaseConnect: any, location: any): Promise<any> {
    this.mapElement = mapElement;
    this.pleaseConnect = pleaseConnect;
    this.location = {
      lat: location.lat,
      lng: location.lng,
      name: location.name
    };
    return this.loadGoogleMaps();
  }

  loadGoogleMaps(): Promise<any> {
    return new Promise(resolve => {
      if (typeof google == "undefined" || typeof google.maps == "undefined") {
        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap();

        if (this.connectivityService.isOnline()) {
          window["mapInit"] = () => {
            this.initMap().then(() => {
              resolve(true);
            });

            this.enableMap();
          };

          let script = document.createElement("script");
          script.id = "googleMaps";

          if (this.configure.googleApiKey) {
            script.src =
              "http://maps.google.com/maps/api/js?key=" +
              this.configure.googleApiKey +
              "&callback=mapInit&libraries=places";
          } else {
            script.src = "http://maps.google.com/maps/api/js?callback=mapInit";
          }

          document.body.appendChild(script);
        }
      } else {
        if (this.connectivityService.isOnline()) {
          this.initMap();
          this.enableMap();
        } else {
          this.disableMap();
        }

        resolve(true);
      }

      this.addConnectivityListeners();
    });
  }

  initMap(): Promise<any> {
    this.mapInitialised = true;

    return new Promise(resolve => {
      if (this.onDevice) {
        const config: BackgroundGeolocationConfig = {
          desiredAccuracy: 10,
          stationaryRadius: 20,
          distanceFilter: 30,
          stopOnTerminate: false // enable this to clear background location settings when the app terminates
        };
        this.backgroundGeolocation.configure(config).then(() => {
          this.backgroundGeolocation
            .on(BackgroundGeolocationEvents.location)
            .subscribe((position: BackgroundGeolocationResponse) => {
              debugger;
              console.log(position);
              console.log(this.location);
              this.location.lat =
                this.location.lat == null
                  ? position.latitude
                  : this.location.lat;
              this.location.lng =
                this.location.lng == null
                  ? position.longitude
                  : this.location.lng;
              console.log(this.location);
              let latLng = new google.maps.LatLng(
                this.location.lat,
                this.location.lng
              );
              let mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              };

              this.map = new google.maps.Map(this.mapElement, mapOptions);
              this.backgroundGeolocation.finish(); // FOR IOS ONLY
            });
        });
        // start recording location
        this.backgroundGeolocation.start();

        // If you wish to turn OFF background-tracking, call the #stop method.
        this.backgroundGeolocation.stop();
        resolve(true);
      } else {
        this.geolocation.getCurrentPosition().then(position => {
          let latLng = new google.maps.LatLng(
            this.location.lat == null
              ? position.coords.latitude
              : this.location.lat,
            this.location.lng == null
              ? position.coords.longitude
              : this.location.lng
          );

          let mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };

          this.map = new google.maps.Map(this.mapElement, mapOptions);
          resolve(true);
        });
      }
    });
  }

  disableMap(): void {
    if (this.pleaseConnect) {
      this.pleaseConnect.style.display = "block";
    }
  }

  enableMap(): void {
    if (this.pleaseConnect) {
      this.pleaseConnect.style.display = "none";
    }
  }

  addConnectivityListeners(): void {
    this.connectivityService.watchOnline().subscribe(() => {
      setTimeout(() => {
        if (typeof google == "undefined" || typeof google.maps == "undefined") {
          this.loadGoogleMaps();
        } else {
          if (!this.mapInitialised) {
            this.initMap();
          }

          this.enableMap();
        }
      }, 2000);
    });

    this.connectivityService.watchOffline().subscribe(() => {
      this.disableMap();
    });
  }
}
