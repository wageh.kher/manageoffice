import { Injectable } from "@angular/core";
import { configurationProvider } from "src/assets/configurtion";
import {
  LocalNotifications,
  ILocalNotificationTrigger
} from "@ionic-native/local-notifications/ngx";
import { InterviewVm } from "../models/InterviewVm";
import { RemindarVm } from "../models/RemindarVm";
@Injectable({
  providedIn: "root"
})
export class HelperService {
  constructor(
    public config: configurationProvider,
    private localNotifications: LocalNotifications
  ) {}

  setLocalNotificationTrigger(
    remainderViewModel: RemindarVm,
    remindarDate: Date
  ) {
    let localNotificationTrigger: ILocalNotificationTrigger = null;
    if (remainderViewModel.TimePerMinute > 0) {
      remindarDate.setMinutes(
        remindarDate.getMinutes() - remainderViewModel.TimePerMinute
      );
    } else if (remainderViewModel.TimePerDay > 0) {
      remindarDate.setDate(
        remindarDate.getDate() - remainderViewModel.TimePerDay
      );
    } else if (remainderViewModel.TimePerWeek > 0) {
      remindarDate.setDate(
        remindarDate.getDate() - remainderViewModel.TimePerWeek * 7
      );
    } else if (remainderViewModel.TimePerMonth > 0) {
      remindarDate.setMonth(
        remindarDate.getMonth() - remainderViewModel.TimePerMonth
      );
    } else if (remainderViewModel.TimePerYear > 0) {
      remindarDate.setFullYear(
        remindarDate.getFullYear() - remainderViewModel.TimePerYear
      );
    }
    localNotificationTrigger = {
      at: remindarDate
    };
    return localNotificationTrigger;
  }

  setScheduleNotification(
    interviewVm: InterviewVm,
    localNotificationTrigger: ILocalNotificationTrigger
  ) {
    this.localNotifications.schedule({
      id: 1,
      title: "Manage Office App",
      text:
        " نعلنكم انه تم تحديد موعد لإجراء مقابلة معكم من " +
        interviewVm.GehaName +
        " ممثل لها " +
        interviewVm.JobName +
        " /" +
        interviewVm.Name +
        "  فى تاريخ " +
        new Date(interviewVm.Date).toLocaleDateString() +
        "  الساعة " +
        this.formatTimeSpan(interviewVm.AttendanceTime) +
        (interviewVm.LocationDesc = null
          ? ""
          : " موقع المقابلة " + interviewVm.LocationDesc),
      trigger: localNotificationTrigger,
      foreground: true // Show the notification while app is open
    });
  }
  formatTimeSpan(currentTime) {
    if (currentTime instanceof Date) {
      currentTime = currentTime.toTimeString().split(" ")[0];
    }
    currentTime = currentTime
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [currentTime];

    if (currentTime.length > 1) {
      // If time format correct
      currentTime = currentTime.slice(1); // Remove full string match value
      currentTime[4] = "";
      currentTime[3] = "";
      currentTime[5] = +currentTime[0] < 12 ? " ص " : " م "; // Set AM/PM
      currentTime[0] = +currentTime[0] % 12 || 12; // Adjust hours
    }
    return currentTime.join(""); // return adjusted time or original string
  }
}
