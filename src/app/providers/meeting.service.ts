import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { configurationProvider } from "src/assets/configurtion";
import { LoadingController } from "@ionic/angular";
import { MeetingVm } from "../models/MeetingVm";
import { MeetingSm } from "../models/MeetingSm";
@Injectable({
  providedIn: "root"
})
export class MeetingService {
  currentStatus: any = null;
  public MeetingsList: MeetingVm[];
  loading: any = null;
  public pageSize: number;

  constructor(
    private http: HttpClient,
    public config: configurationProvider,
    private loadingCtrl: LoadingController
  ) {
    this.MeetingsList = [];
    this.pageSize = 2;
  }
  setcurrentStatus(currentStatus: number) {
    this.currentStatus = currentStatus;
  }

  async FindAllMeetings(pageIndex: number) {
    this.MeetingsList = [];
    const searchModel: MeetingSm = {
      Titles: null,
      MeetingStatusIds: this.currentStatus,
      AttendanceTime: null,
      IsMakeSound: null,
      DepartmentId: null,
      PageSize: this.pageSize,
      Page: pageIndex,
      DaysNo: 30,
      FromDate: new Date(),
      TodayDate: null
    };
    const apiURL =
      this.config.serverUrl +
      this.config.serverPort +
      this.config.Api.getAllMeetings;

    this.loading = await this.loadingCtrl.create({
      message: `<img src="/assets/icon/imgs/loaderIcon.png"  >`
    });
    this.loading.present();

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json"
    });
    const headerOptions = {
      headers: headers
    };
    const _res = this.http
      .post(apiURL, searchModel, headerOptions)
      .toPromise()
      .finally(() => {});
    this.loading.dismiss();
    return _res;
  }

  async SaveMeeting(meetingModel: MeetingVm) {
    const apiURL =
      this.config.serverUrl +
      this.config.serverPort +
      this.config.Api.saveMeeting;
    this.loading = await this.loadingCtrl.create({
      message: `<img src="/assets/icon/imgs/loaderIcon.png"  >`
    });
    this.loading.present();

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json"
    });
    const headerOptions = {
      headers: headers
    };

    const _subscribtion = await this.http
      .post(apiURL, meetingModel, headerOptions)
      .toPromise();
    this.loading.dismiss();
    return _subscribtion;
  }
}
