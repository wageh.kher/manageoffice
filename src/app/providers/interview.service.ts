import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { configurationProvider } from "src/assets/configurtion";
import { LoadingController } from "@ionic/angular";
import {
  LocalNotifications,
  ILocalNotificationTrigger
} from "@ionic-native/local-notifications/ngx";
import { InterviewVm } from "../models/InterviewVm";
import { InterviewSm } from "../models/InterviewSm";
import { InterviewChangeStatusVm } from "../models/InterviewChangeStatusVm";
import { RemindarVm } from "../models/RemindarVm";
@Injectable({
  providedIn: "root"
})
export class InterviewService {
  currentStatus: any = null;
  public interviewsList: InterviewVm[];
  loading: any = null;
  public pageSize: number;

  constructor(
    private http: HttpClient,
    public config: configurationProvider,
    private loadingCtrl: LoadingController,
    private localNotifications: LocalNotifications
  ) {
    this.interviewsList = [];
    this.pageSize = 2;
  }
  setcurrentStatus(currentStatus: number) {
    this.currentStatus = currentStatus;
  }

  async FindAllInterviews(pageIndex: number) {
    this.interviewsList = [];
    const searchModel: InterviewSm = {
      GehaName: null,
      JobIds: null,
      InterviewStatusIds: this.currentStatus,
      Name: null,
      AttendanceTime: null,
      TodayDate: null,
      IsMakeSound: null,
      DepartmentId: null,
      PageSize: this.pageSize,
      Page: pageIndex
    };
    const apiURL =
      this.config.serverUrl +
      this.config.serverPort +
      this.config.Api.getAllInterviews;

    this.loading = await this.loadingCtrl.create({
      message: `<img src="/assets/icon/imgs/loaderIcon.png"  >`
    });
    this.loading.present();

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json"
    });
    const headerOptions = {
      headers: headers
    };
    const _res = this.http
      .post(apiURL, searchModel, headerOptions)
      .toPromise()
      .finally(() => {});
    this.loading.dismiss();
    return _res;
  }

  async ChangeStatus(
    interviewId: number,
    requestStatusId: number,
    departmentId: number
  ) {
    const viewModel: InterviewChangeStatusVm = {
      InterviewId: interviewId,
      RequestStatusId: requestStatusId,
      DepartmentId: departmentId
    };
    const apiURL =
      this.config.serverUrl +
      this.config.serverPort +
      this.config.Api.interviewChangeStatus;
    this.loading = await this.loadingCtrl.create({
      message: `<img src="/assets/icon/imgs/loaderIcon.png"  >`
    });
    this.loading.present();

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json"
    });
    const headerOptions = {
      headers: headers
    };

    const _subscribtion = await this.http
      .post(apiURL, viewModel, headerOptions)
      .toPromise()
      .finally(() => this.loading.dismiss());
    this.loading.dismiss();
    return _subscribtion;
  }

  async FindInterviewSettings() {
    const apiURL =
      this.config.serverUrl +
      this.config.serverPort +
      this.config.Api.FindInterviewSettings;
    this.loading = await this.loadingCtrl.create({
      message: `<img src="/assets/icon/imgs/loaderIcon.png"  >`
    });
    this.loading.present();

    const headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    const headerOptions = {
      headers: headers
    };
    const _res = this.http.get(apiURL, headerOptions).toPromise();
    this.loading.dismiss();
    return _res;
  }

  async SaveInterview(interviewModel: InterviewVm) {
    const apiURL =
      this.config.serverUrl +
      this.config.serverPort +
      this.config.Api.saveInterview;
    this.loading = await this.loadingCtrl.create({
      message: `<img src="/assets/icon/imgs/loaderIcon.png"  >`
    });
    this.loading.present();

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json"
    });
    const headerOptions = {
      headers: headers
    };

    const _subscribtion = await this.http
      .post(apiURL, interviewModel, headerOptions)
      .toPromise();
    this.loading.dismiss();
    return _subscribtion;
  }
}
