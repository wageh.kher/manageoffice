import { Component, OnInit } from "@angular/core";
import { configurationProvider } from "src/assets/configurtion";
import { InterviewService } from "../../providers/interview.service";

@Component({
  selector: "app-newRoot",
  templateUrl: "./new.page.html",
  styleUrls: ["./new.page.scss"]
})
export class NewPage implements OnInit {
  constructor(
    private interviewService: InterviewService,
    public config: configurationProvider
  ) {
    this.interviewService.setcurrentStatus(this.config.InterviewStatus.new);
  }

  ngOnInit() {}
}
