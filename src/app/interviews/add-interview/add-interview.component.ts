import { Component, OnInit } from "@angular/core";
import { ModalController, NavParams, ToastController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { InterviewService } from "../../providers/interview.service";
import { IonicSelectableComponent } from "ionic-selectable";
import { mobiscroll } from "src/lib/mobiscroll/js/mobiscroll.angular.min";
import { LocationSelectComponent } from "src/app/Pages/location-select/location-select.component";
import { SelectListItem } from "src/app/models/SelectListItem";
import { RemindarVm } from "src/app/models/RemindarVm";
import { InterviewVm } from "src/app/models/InterviewVm";
import { HelperService } from "src/app/providers/heper.service";

mobiscroll.settings = {
  lang: "ar",
  theme: "ios"
};

@Component({
  selector: "app-add-interview",
  templateUrl: "./add-interview.component.html",
  styleUrls: ["./add-interview.component.scss"]
})
export class AddInterviewComponent {
  public interviewModal: InterviewVm;
  jobs: SelectListItem[];
  departments: SelectListItem[];
  remindars: RemindarVm[];
  repetitions: SelectListItem[];
  currentDepartment: SelectListItem;
  currentJob: SelectListItem;
  currentRemindar: RemindarVm;
  currentRepetition: SelectListItem;
  constructor(
    public modalCtl: ModalController,
    public translate: TranslateService,
    private toastCtl: ToastController,
    private navParams: NavParams,
    private interviewService: InterviewService,
    private helperService: HelperService
  ) {
    this.jobs = [];
    this.departments = [];
    this.remindars = [];
    this.repetitions = [];
    this.interviewModal = this.navParams.get("interviewModal");
    this.FindInterviewSettings();
  }

  onDismiss() {
    this.modalCtl.dismiss();
  }

  FindInterviewSettings() {
    return this.interviewService.FindInterviewSettings().then((data: any) => {
      // 1:: Jobs
      if (data.result.jobs.length > 0) {
        data.result.jobs.map(job =>
          job.Value === "" ? "" : this.jobs.push(job)
        );
        console.log("::  Get All Jobs ::");
        console.log(this.jobs);
        this.currentJob = data.result.jobs.filter(
          c => c.Value == this.interviewModal.JobId
        )[0];
        console.log(this.currentJob);
      }
      // 2:: Departments
      if (data.result.departments.length > 0) {
        data.result.departments.map(department =>
          department.Value === "" ? "" : this.departments.push(department)
        );
        console.log("::  Get All Departments  ::");
        console.log(this.departments);
        this.currentDepartment = data.result.departments.filter(
          c => c.Value == this.interviewModal.DepartmentId
        )[0];
        console.log(this.currentDepartment);
      }

      // 3:: Remindars
      if (data.result.remindars.length > 0) {
        data.result.remindars.map(remindar =>
          remindar.Id === "" ? "" : this.remindars.push(remindar)
        );
        console.log("::  Get All Remindars  ::");
        console.log(this.remindars);
        this.currentRemindar = data.result.remindars.filter(
          c => c.Id == this.interviewModal.RemindarId
        )[0];
        console.log(this.currentRemindar);
      }

      // 4:: Repetition
      if (data.result.repetitions.length > 0) {
        data.result.repetitions.map(repetition =>
          repetition.Value === "" ? "" : this.repetitions.push(repetition)
        );
        console.log("::  Get All Departments  ::");
        console.log(this.repetitions);
        this.currentRepetition = data.result.repetitions.filter(
          c => c.Value == this.interviewModal.RepetitionId
        )[0];
        console.log(this.currentRepetition);
      }
    });
  }

  onSaveInterview(formModel) {
    let localNotificationTrigger = null;
    let remindarDate = null;
    const attendanceTime =
      formModel.form.controls.AttendanceTime.value instanceof Date
        ? formModel.form.controls.AttendanceTime.value
            .toTimeString()
            .split(" ")[0]
        : formModel.form.controls.AttendanceTime.value;
    if (this.currentRemindar != null) {
      remindarDate = formModel.form.controls.Date.value;
      remindarDate = new Date(remindarDate);
      const attendanceTimeArr = attendanceTime.split(":");
      remindarDate.setHours(attendanceTimeArr[0]);
      remindarDate.setMinutes(attendanceTimeArr[1]);
      localNotificationTrigger = this.helperService.setLocalNotificationTrigger(
        this.currentRemindar,
        remindarDate
      );
    }

    const viewModal: InterviewVm = {
      Id: this.interviewModal.Id,
      GehaName: formModel.form.controls.GehaName.value,
      JobId: formModel.form.controls.JobId.value.Value,
      Note: formModel.form.controls.Note.value,
      Name: formModel.form.controls.Name.value,
      Date: new Date(formModel.form.controls.Date.value),
      AttendanceTime: attendanceTime,
      AttendeesNumber: formModel.form.controls.AttendeesNumber.value,
      DepartmentId: formModel.form.controls.DepartmentId.value.Value,
      RemindarId:
        formModel.form.controls.RemindarId.value == undefined
          ? null
          : formModel.form.controls.RemindarId.value.Id,
      RepetitionId:
        formModel.form.controls.RepetitionId.value == undefined
          ? null
          : formModel.form.controls.RepetitionId.value.Value,
      IsSendMail:
        formModel.form.controls.IsSendMail.value == null
          ? false
          : formModel.form.controls.IsSendMail.value,
      RemindarDate:
        formModel.form.controls.RemindarId.value == undefined
          ? null
          : remindarDate,
      RemindarTime:
        formModel.form.controls.RemindarId.value == undefined
          ? null
          : localNotificationTrigger.at.toTimeString().split(" ")[0],
      RepetitionCount:
        formModel.form.controls.RepetitionId.value == undefined ||
        formModel.form.controls.RepetitionId.value.Value == 1
          ? null
          : formModel.form.controls.RepetitionCount.value,
      LocationDesc: formModel.form.controls.LocationDesc.value,
      LocationLat: this.interviewModal.LocationLat,
      LocationLong: this.interviewModal.LocationLong
    };

    this.interviewService.SaveInterview(viewModal).then(async (data: any) => {
      console.log(":: Save Interview Done ! ::");
      var messageText = "";
      if (data.RowsCount == 0 || data.result.RowsCount == 0) {
        this.translate.get("messages").subscribe(async value => {
          messageText = value.fail;
        });
      } else if (data.result.RowsCount > 0) {
        this.translate.get("messages").subscribe(async value => {
          messageText = value.save;
        });
        if (localNotificationTrigger != null) {
          viewModal.JobName = this.interviewModal.JobName;
          this.helperService.setScheduleNotification(
            viewModal,
            localNotificationTrigger
          );
        }
        this.modalCtl.dismiss();
      }
      const toast = await this.toastCtl.create({
        message: messageText,
        duration: 2000
      });
      toast.present();
    });
  }
  jobChange(event: { component: IonicSelectableComponent; value: any }) {
    console.log("job:", event.value);
    this.interviewModal.JobId = event.value.Value;
    this.interviewModal.JobName = event.value.Text;
  }
  departmentChange(event: { component: IonicSelectableComponent; value: any }) {
    console.log("department:", event.value);
    this.interviewModal.DepartmentId = event.value.Value;
    this.interviewModal.DepartmentName = event.value.Text;
  }

  remindarChange(event: { component: IonicSelectableComponent; value: any }) {
    console.log("remindar:", event.value);
    this.interviewModal.RemindarId = event.value.Id;
    this.interviewModal.RemindarName = event.value.RemindarDescription;
    this.currentRemindar = this.remindars.filter(
      c => c.Id == this.interviewModal.RemindarId
    )[0];
  }
  repetitionChange(event: { component: IonicSelectableComponent; value: any }) {
    console.log("repetition:", event.value);
    this.interviewModal.RepetitionId = event.value.Value;
    this.interviewModal.RepetitionName = event.value.Text;
  }
  async launchLocationPage() {
    const modal = await this.modalCtl.create({
      component: LocationSelectComponent,
      componentProps: { interviewModal: this.interviewModal }
    });

    modal.onDidDismiss().then((location: any) => {
      if (location.data != undefined) {
        this.interviewModal.LocationLat = location.data.lat;
        this.interviewModal.LocationLong = location.data.lng;
        this.interviewModal.LocationDesc = location.data.name;
      } else {
        this.interviewModal.LocationLat = null;
        this.interviewModal.LocationLong = null;
        this.interviewModal.LocationDesc = null;
      }
    });

    modal.present();
  }
}
