import { Component, OnInit, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { AllPage } from "./all/all.page";
import { NewPage } from "./new/new.page";
import { AwaitingPage } from "./awaiting/awaiting.page";
import { RejectedPage } from "./rejected/rejected.page";
import { AcceptedPage } from "./accepted/accepted.page";
import { ModalController, IonTabs } from "@ionic/angular";
import { AddInterviewComponent } from "./add-interview/add-interview.component";
import { SwipeTabDirective } from "../directives/swipe-tab.directive";

import { configurationProvider } from "src/assets/configurtion";
import { InterviewService } from "../providers/interview.service";
@Component({
  selector: "app-interviews",
  templateUrl: "./interviews.page.html",
  styleUrls: ["./interviews.page.scss"]
})
export class InterviewsPage implements OnInit {
  @ViewChild(SwipeTabDirective) swipeTabDirective: SwipeTabDirective;
  @ViewChild("interviewTabs") tabRef: IonTabs;
  allRoot = AllPage;
  newRoot = NewPage;
  awaitingRoot = AwaitingPage;
  rejectedRoot = RejectedPage;
  acceptedRoot = AcceptedPage;
  constructor(
    public translate: TranslateService,
    public modalCtl: ModalController,
    private interviewService: InterviewService,
    public config: configurationProvider
  ) {}

  ngOnInit() {}
  ionTabsDidChange($event) {
    console.log("[TabsPage] ionTabsDidChange, $event: ", $event);
    this.swipeTabDirective.onTabInitialized($event.tab);
    const currentStatus = $event.tab.replace("Root", "");
    this.interviewService.setcurrentStatus(
      this.config.InterviewStatus[currentStatus == "all" ? null : currentStatus]
    );
  }

  onTabChange($event) {
    console.log("[TabsPage] onTabChange, $event: ", $event);
    this.tabRef.select($event);
  }

  async onAddInterview() {
    const modal = await this.modalCtl.create({
      component: AddInterviewComponent,
      componentProps: { interviewModal: [] }
    });
    await modal.present();
  }
}
