import { Component, OnInit, ViewChild } from "@angular/core";
import {
  PopoverController,
  IonInfiniteScroll,
  ToastController,
  AlertController
} from "@ionic/angular";
import { OptionsPopoverPage } from "../../Pages/options-popover/options-popover.page";

import { configurationProvider } from "src/assets/configurtion";
import { TranslateService } from "@ngx-translate/core";
import { InterviewVm } from "src/app/models/InterviewVm";
import { InterviewService } from "src/app/providers/interview.service";
import { HelperService } from "src/app/providers/heper.service";

@Component({
  selector: "app-interview-card",
  templateUrl: "./interview-card.component.html",
  styleUrls: ["./interview-card.component.scss"]
})
export class InterviewCardComponent implements OnInit {
  private interviews: InterviewVm[];
  private pageIndex: number;
  scheduled = [];
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  ngOnInit() {
    //console.log("::  Get Init  component::");
  }
  constructor(
    private popoverCtrl: PopoverController,
    public interviewService: InterviewService,
    public config: configurationProvider,
    public translate: TranslateService,
    private toastCtl: ToastController,
    private alertCtrl: AlertController,
    public helperService: HelperService
  ) {
    //console.log("::  Get constructor  component::");
    this.interviews = [];
    this.pageIndex = 1;
    this.onFindAllInterviews();
  }

  onFindAllInterviews() {
    if (this.interviews.length > 0) {
      this.interviews = [];
    }
    this.pageIndex = 1;
    return this.interviewService
      .FindAllInterviews(this.pageIndex)
      .then((data: any) => {
        if (data.result.length > 0) {
          data.result.map(interview => this.interviews.push(interview));
          console.log("::  Get All Interviews ::");
          console.log(this.interviews);
          return this.interviews;
        }
      });
  }

  async onToggleOptions(ev: any, interviewVm: InterviewVm) {
    const modal = await this.popoverCtrl.create({
      component: OptionsPopoverPage,
      event: ev,
      componentProps: { interviewModal: interviewVm }
    });
    await modal.present();
  }

  async onChangeStatus(
    interviewId: number,
    requestStatusId: number,
    departmentId: number
  ) {
    const alert = await this.alertCtrl.create({
      header: "تأكيد",
      message: "هل تريد تغير الحالة بالفعل ؟",
      buttons: [
        {
          text: "الغاء",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "موافق",
          handler: () => {
            this.interviewService
              .ChangeStatus(interviewId, requestStatusId, departmentId)
              .then(async (data: any) => {
                console.log(":: Status Change Done ! ::");
                var messageText = "";
                if (data.result.RowsCount > 0) {
                  this.translate.get("messages").subscribe(async value => {
                    messageText = value.save;
                  });
                } else {
                  this.translate.get("messages").subscribe(async value => {
                    messageText = value.fail;
                  });
                }
                const toast = await this.toastCtl.create({
                  message: messageText,
                  duration: 2000
                });
                toast.present();
              })
              .finally(async () => {
                const infScroll = await this.infiniteScroll;
                infScroll.disabled = false;
                this.onFindAllInterviews();
              });
          }
        }
      ]
    });
    alert.present();
  }

  OnLoadData(event) {
    setTimeout(() => {
      event.target.complete();
      this.pageIndex = this.pageIndex + 1;
      this.interviewService
        .FindAllInterviews(this.pageIndex)
        .then((data: any) => {
          if (
            data.result == null ||
            data.result.length < this.interviewService.pageSize
          ) {
            event.target.disabled = true;
          }

          if (data.result.length > 0) {
            data.result.map(interview => this.interviews.push(interview));

            return this.interviews;
          }
        });
    }, 500);
  }
}
