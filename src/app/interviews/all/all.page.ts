import { Component, OnInit } from "@angular/core";
import { configurationProvider } from "src/assets/configurtion";
import { InterviewService } from "src/app/providers/interview.service";
@Component({
  selector: "app-allRoot",
  templateUrl: "./all.page.html",
  styleUrls: ["./all.page.scss"]
})
export class AllPage implements OnInit {
  constructor(
    private interviewService: InterviewService,
    public config: configurationProvider
  ) {
    this.interviewService.setcurrentStatus(null);
  }
  ngOnInit() {}
}
