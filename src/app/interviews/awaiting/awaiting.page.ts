import { Component, OnInit } from "@angular/core";
import { configurationProvider } from "src/assets/configurtion";
import { InterviewService } from "src/app/providers/interview.service";

@Component({
  selector: "app-awaitingRoot",
  templateUrl: "./awaiting.page.html",
  styleUrls: ["./awaiting.page.scss"]
})
export class AwaitingPage implements OnInit {
  constructor(
    private interviewService: InterviewService,
    public config: configurationProvider
  ) {
    this.interviewService.setcurrentStatus(
      this.config.InterviewStatus.awaiting
    );
  }

  ngOnInit() {}
}
