import { Component, OnInit } from "@angular/core";
import { configurationProvider } from "src/assets/configurtion";
import { InterviewService } from "../../providers/interview.service";

@Component({
  selector: "app-rejectedRoot",
  templateUrl: "./rejected.page.html",
  styleUrls: ["./rejected.page.scss"]
})
export class RejectedPage implements OnInit {
  constructor(
    private interviewService: InterviewService,
    public config: configurationProvider
  ) {
    this.interviewService.setcurrentStatus(
      this.config.InterviewStatus.rejected
    );
  }

  ngOnInit() {}
}
