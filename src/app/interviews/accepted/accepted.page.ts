import { Component, OnInit } from "@angular/core";
import { configurationProvider } from "src/assets/configurtion";
import { InterviewService } from "src/app/providers/interview.service";
@Component({
  selector: "app-acceptedRoot",
  templateUrl: "./accepted.page.html",
  styleUrls: ["./accepted.page.scss"]
})
export class AcceptedPage implements OnInit {
  ngOnInit() {}
  constructor(
    private interviewService: InterviewService,
    public config: configurationProvider
  ) {
    this.interviewService.setcurrentStatus(
      this.config.InterviewStatus.accepted
    );
  }
}
