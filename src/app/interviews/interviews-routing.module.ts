import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { InterviewsPage } from "./interviews.page";
import { AllPage } from "./all/all.page";
import { AcceptedPage } from "./accepted/accepted.page";
import { RejectedPage } from "./rejected/rejected.page";
import { AwaitingPage } from "./awaiting/awaiting.page";
import { NewPage } from "./new/new.page";

const routes: Routes = [
  {
    path: "",
    component: InterviewsPage,
    children: [
      {
        path: "allRoot",
        component: AllPage
      },
      {
        path: "acceptedRoot",
        component: AcceptedPage
      },
      {
        path: "newRoot",
        component: NewPage
      },
      {
        path: "awaitingRoot",
        component: AwaitingPage
      },
      {
        path: "rejectedRoot",
        component: RejectedPage
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterviewsRountingPageModule {}
