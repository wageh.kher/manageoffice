import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { IonicModule } from "@ionic/angular";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { InterviewsRountingPageModule } from "./interviews-routing.module";
import { OptionsPopoverPageModule } from "../Pages/options-popover/options-popover.module";
import { InterviewsPage } from "./interviews.page";
import { AllPage } from "./all/all.page";
import { AcceptedPage } from "./accepted/accepted.page";
import { InterviewCardComponent } from "./interview-card/interview-card.component";
import { NewPage } from "./new/new.page";
import { AwaitingPage } from "./awaiting/awaiting.page";
import { RejectedPage } from "./rejected/rejected.page";
import { DirectivesModule } from "../directives/directives.module";
import { InterviewService } from "../providers/interview.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    InterviewsRountingPageModule,
    OptionsPopoverPageModule,
    HttpClientModule,
    DirectivesModule
  ],
  declarations: [
    InterviewsPage,
    AllPage,
    AcceptedPage,
    NewPage,
    AwaitingPage,
    RejectedPage,
    InterviewCardComponent
  ],
  entryComponents: [InterviewCardComponent],
  providers: [HttpClient, InterviewService, InterviewCardComponent]
})
export class InterviewsPageModule {}
