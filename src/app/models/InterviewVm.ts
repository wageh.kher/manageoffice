import { Time } from "@angular/common";

export interface InterviewVm {
  Id: number;
  GehaName: string;
  JobId: number;
  RequestStatusNameFl?: string;
  RequestStatusName?: string;
  Note?: string;
  Name: string;
  Date: Date;
  AttendanceTime: Time;
  JobName?: string;
  AttendeesNumber: number;
  RequestStatusId?: number;
  DepartmentId: number;
  DepartmentName?: string;
  RemindarId?: number;
  RemindarName?: string;
  RepetitionId?: number;
  RepetitionName?: string;
  IsSendMail?: boolean;
  RemindarDate?: Date;
  RemindarTime?: Time;
  RepetitionCount?: number;
  LocationLat?: number;
  LocationLong?: number;
  LocationDesc?: string;
}
