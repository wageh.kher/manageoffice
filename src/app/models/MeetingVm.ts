import { Time } from "@angular/common";
import { MeetingMemberVm } from "./MeetingMemberVm";

export interface MeetingVm {
  Id: number;
  Titles: string;
  MeetingDate?: Date;
  AttendanceTime?: Time;
  AlarmMinutes?: number;
  AttendeesNumber?: number;
  Address?: string;
  MeetingObjectives?: string;
  Note?: string;
  IsDone?: boolean;
  IsChangeStatus?: boolean;
  DepartmentId?: number;
  MeetingStatusId?: number;
  DepartmentName?: string;
  StatusName?: string;
  StatusColor?: string;
  MeetingGroupId?: number;
  LocationLat?: number;
  LocationLong?: number;
  LocationDesc?: string;
  RegisterId?: number;
  RemindarId?: number;
  RemindarName?: string;
  RepetitionId?: number;
  RepetitionName?: string;
  RemindarDate?: Date;
  RemindarTime?: Time;
  RepetitionCount?: number;
  IsAllDay?: boolean;
  UserId?: number;
  SaveAsTemplate?: boolean;
  PrivacyId?: number;
  IsBusy?: boolean;
  MeetingMembers: MeetingMemberVm[];
}
