export interface SelectListItem {
  Selected: boolean;
  Text: string;
  Value: string;
}
