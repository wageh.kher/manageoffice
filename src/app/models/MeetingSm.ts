import { Time } from "@angular/common";

export interface MeetingSm {
  Titles: string;
  MeetingStatusIds?: number;
  AttendanceTime?: Time;
  IsMakeSound?: boolean;
  DepartmentId?: number;
  PageSize: number;
  //PageIndex
  Page: number;
  DaysNo?: number;
  FromDate?: Date;
  TodayDate?: Date;
}
