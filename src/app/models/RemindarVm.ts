export interface RemindarVm {
  Id: number;
  RemindarDescription: string;
  TimePerMinute?: number;
  TimePerDay?: number;
  TimePerWeek?: number;
  TimePerMonth?: number;
  TimePerYear?: number;
}
