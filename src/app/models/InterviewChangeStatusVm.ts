import { Time } from "@angular/common";

export interface InterviewChangeStatusVm {
  InterviewId: number;
  RequestStatusId: number;
  DepartmentId: number;
}
