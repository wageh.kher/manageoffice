import { Time } from "@angular/common";

export interface MeetingMemberVm {
  Id: number;
  MeetingId: number;
  JobId: number;
  Name: string;
  Phone: string;
  Telphone: string;
  Email: string;
  IsAttend: boolean;
  JobName: string;
  MeetingMemberRole: string;
  Status: string;
  MemberId: number;
  Address: string;
}
