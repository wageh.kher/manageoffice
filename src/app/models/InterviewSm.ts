import { Time } from "@angular/common";

export interface InterviewSm {
  GehaName: string;
  JobIds?: number;
  InterviewStatusIds?: number;
  Name: string;
  AttendanceTime?: Time;
  TodayDate?: Date;
  IsMakeSound?: boolean;
  DepartmentId?: number;
  PageSize: number;
  //PageIndex
  Page: number;
}
