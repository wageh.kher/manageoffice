import { Component, Inject } from "@angular/core";

import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { TranslateService } from "@ngx-translate/core";
import { DOCUMENT } from "@angular/platform-browser";
@Component({
  selector: "app-root",
  templateUrl: "app.component.html"
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.initializeApp();
    this.document.documentElement.dir = "rtl";
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // this.platform.setDir("rtl", true);
      // this.platform.setLang("ar", true);
      this.translate.setDefaultLang("ar");
      this.translate.use("ar");
    });
  }

  // setDirectionTo(dir: Direction) {
  //   this.document.documentElement.dir = dir;
  // }
}
