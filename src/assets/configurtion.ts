import { Injectable } from "@angular/core";

@Injectable()
export class configurationProvider {
  // public serverUrl: string = "http://localhost";
  // public serverPort: string = ":5484";
  //public serverUrl: string = "http://192.168.43.14";
  //public serverPort: string = ":45457";

  public serverUrl: string = "http://mofficeapi.5twattech.net";
  public serverPort: string = "";
  public Currency: string = "EG";
  public Api: any = {
    FindInterviewSettings: "/api/ManageInterviews/FindInterviewSettings",
    getAllInterviews: "/api/ManageInterviews/FindInterviews",
    interviewChangeStatus: "/api/ManageInterviews/InterviewChangeStatus",
    saveInterview: "/api/ManageInterviews/Save",
    getAllMeetings: "/api/ManageMeeting/FindMeeting",
    saveMeeting: "/api/ManageMeeting/SaveMeeting",
    meetingChangeStatus: "/api/ManageMeeting/MeetingChangeStatus",
    updateMeeting: "/api/ManageMeeting/UpdateMeeting",
    UpdateAttendMember: "/api/ManageMeeting/UpdateAttendMember",
    saveJob: "/api/ManageMeeting/SaveJob",
    saveMeetingMember: "/api/ManageMeeting/SaveMeetingMember",
    saveGroups: "/api/ManageMeeting/SaveGroups",
    saveTasks: "/api/ManageMeeting/SaveTasks"
  };
  public googleApiKey: string = "AIzaSyA5Tr_IBz_4h0Fhk3UYDYTEZ3GQ4lqswW0";
  public InterviewStatus: any = {
    new: 1,
    accepted: 2,
    awaiting: 3,
    rejected: 4,
    done: 5,
    edit: 6,
    delete: 7
  };
}
